package Task11_3;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class UtilsTest {
    Utils utils;
    @BeforeMethod
    public void setUp(){
        utils  = new Utils();
    }

    @DataProvider(name = "numberOnCheck")
    public Object[][] dataProviderForIsEvenNumber() {
        return new Object[][]{
                {2, true},
                {3, false},
                {20, true},
                {21, false}
        };
    }

    @Test(dataProvider = "numberOnCheck")
    public void testEvenOrNotEvenNumber(int number, boolean expectedResult) {
        boolean actualResult = utils.isEvenHumber(number);
        Assert.assertEquals(actualResult, expectedResult, "Число %d не четное".formatted(number));
    }

    @DataProvider(name = "email")
    public Object[][] dataProviderForCheckCorrectEmail() {
        return new Object[][]{
                {"test@test.ru", "проверено!"},
                {"test@testru", "Email-адрес некорректный!"},
                {"testtest.ru", "Email-адрес некорректный!"},
                {"test", "Email-адрес некорректный!"}
        };
    }

    @Test(dataProvider = "email")
    public void testCorrectEmail(String email, String expectedResult) {
        try {
            String actualResult = utils.checkCorrectEmail(email);
            Assert.assertEquals(actualResult, expectedResult);
        } catch (IncorrectEmailException e) {
            String actualResult = e.getMessage();
            Assert.assertEquals(actualResult, expectedResult);
        }
    }

    @DataProvider(name = "listWithStrings")
    public Object[][] dataProviderForListWithFormattedStrings() {
        return new Object[][]{
                {Arrays.asList("qwertyu", "qwert", "assssssssssssssss", "fdgdfgdfgdf", "ffd", "asdf"),
                        Arrays.asList("qwert", "assssssssssssssss", "ffd", "asdf")},
                {Arrays.asList("qwertyu", "qwert", "1111111111111", "atyrytrutr7dt7t7", "123", "12345"),
                        Arrays.asList("qwert", "atyrytrutr7dt7t7", "123", "12345")},

        };
    }

    @Test(dataProvider = "listWithStrings")
    public void testListWithFormattedStrings(List<String> listString, List<String> expectedResult){
        List<String> actualResult = utils.returnListWithFormattedStrings(listString);
        Assert.assertEquals(actualResult, expectedResult);
    }

}
