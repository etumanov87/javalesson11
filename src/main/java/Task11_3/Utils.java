package Task11_3;

import java.util.List;

public class Utils {
    public String checkCorrectEmail(String email) throws IncorrectEmailException {
        if (email.contains("@") && email.contains(".")) {
            return "проверено!";
        } else {
            throw new IncorrectEmailException("Email-адрес некорректный!");
        }
    }

    public boolean isEvenHumber(Integer number) {
        return number % 2 == 0;
    }

    public List<String> returnListWithFormattedStrings(List<String> stringList) {
        return stringList
                .stream()
                .filter(line -> line.length() < 6 || line.startsWith("a"))
                .toList();
    }
}
