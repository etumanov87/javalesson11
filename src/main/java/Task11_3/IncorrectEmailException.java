package Task11_3;

public class IncorrectEmailException extends Exception {
    public IncorrectEmailException(String s) {
        super(s);
    }
}

