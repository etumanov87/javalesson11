package Task11_3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Utils utils = new Utils();

        try {
            System.out.println(utils.checkCorrectEmail("test@test.ru"));
            System.out.println(utils.checkCorrectEmail("testtest.ru"));
        } catch (IncorrectEmailException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(utils.isEvenHumber(2));
        System.out.println(utils.isEvenHumber(3));


        System.out.println(utils.returnListWithFormattedStrings(
                Arrays.asList("qwertyu", "qwert", "assssssssssssssss", "fdgdfgdfgdf", "ffd", "asdf")));

    }
}
